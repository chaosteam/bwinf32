{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
module Main where

import           Common
import           Control.Applicative
import           Control.Lens
import           Control.Monad
import           Control.Monad.Logic
import           Control.Monad.State
import           System.Exit
import           Data.List
import           Data.Monoid
import qualified Data.Set as S

data Result = Result
            { _resultKeptDates :: S.Set Date
            , _resultEntries :: S.Set Entry
            , _resultIsPartial :: Bool
            , _resultDateCount :: Int
            , _resultTutorCount :: Int
            } deriving Show
makeFields ''Result

type SolveM = StateT RegistrationList Logic

instance Monoid Result where
  mempty = Result S.empty S.empty True 0 0
  mappend r@(Result kd d ip dc tc) r'@(Result kd' d' ip' dc' tc')
    | not ip = r
    | not ip' = r'
    | otherwise = Result (kd <> kd') (d <> d') checkPartial (max dc dc') (max tc tc')
    where checkPartial = S.size (S.union kd kd') < max tc tc' + 1

drawFrom :: (RegistrationList -> [a]) -> SolveM a
drawFrom f = get >>= msum . map return . f

withHiddenDate :: Date -> SolveM a -> SolveM a
withHiddenDate d a = modify (hideDate d) *> a <* modify (unhideDate d)

insertPair :: Entry -> Entry -> Result -> Result
insertPair a b = execState $ ins a >> ins b
  where ins x = do
          entries.contains x .= True
          keptDates.contains (x^.date) .= True
          kd <- use keptDates
          tc <- use tutorCount
          when (S.size kd >= tc + 1) $ isPartial .= False

findPair :: Date -> SolveM (Entry,Entry)
findPair d = do
  entry1 <- drawFrom $ entriesForDate d
  entry2 <- mfilter (/= entry1) $ drawFrom $ entriesForTutor $ entry1^.tutor
  return (entry1, entry2)

startPair :: SolveM (Entry,Entry)
startPair = do
  t <- get
  lift $ msum [return (a,b) | (a:bs) <- tails (entriesForTutor 0 t), b <- bs]

solveM :: Maybe Date -> SolveM Result
solveM c = do
  (a,b) <- maybe startPair findPair c
  modify $ removeTutor (a^.tutor)
  r1 <- withHiddenDate (a^.date) $ solveM (Just $ b^.date) <|> memptyWithDim
  r2 <- withHiddenDate (b^.date) $ solveM (Just $ a^.date) <|> memptyWithDim
  modify $ removeDate (a^.date) . removeDate (b^.date)
  return $ insertPair a b $ r1 <> r2

  where memptyWithDim = do
          dc <- use dateCount
          tc <- use tutorCount
          return $ mempty & dateCount .~ dc & tutorCount .~ tc

solve :: RegistrationList -> [Result]
solve t = observeAll $ evalStateT (mfilter (not . view isPartial) $ solveM Nothing) t

allPossible :: Int -> Int -> RegistrationList
allPossible tc dc = RegistrationList (S.fromList [(t,d) | t <- [0..pred tc], d <- [0..pred dc]]) tc dc S.empty

prettyShowResult :: Result -> String
prettyShowResult res = unlines $ map prettyShowTutor [0..res^.tutorCount - 1]
  where prettyShowTutor t = unwords $ map (prettyShowEntry t) [0..res^.dateCount - 1]
        prettyShowEntry t d 
          | not $ res^.keptDates.contains d = "|"
          | res^.entries.contains (t,d) = "X"
          | otherwise = " "

main :: IO ()
main = do
  contents <- getContents
  let l = readRegistrationList contents
  l' <- case l of
    Left err -> putStrLn ("Fehler: " ++ err) >> exitFailure
    Right r  -> return r
  putStrLn $ replicate 80 '-'
  putStrLn "Eingabe: "
  putStrLn $ prettyShowRegistrationList l'
  putStrLn $ replicate 80 '-'
  case solve l' of
    (h:_) -> do
      putStrLn "Lösung:"
      putStrLn $ prettyShowResult h
      putStrLn $ "Folgende Termine formen eine gültige Anmeldeliste: "
      putStrLn $ unwords $ map show $ S.toList $ h^.keptDates
    _ -> do
      putStrLn "Nicht lösbar"
      exitFailure
