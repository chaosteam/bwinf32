{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
module Common where

import           Control.Error
import           Control.Lens
import           Control.Monad
import           Data.Functor
import qualified Data.Set as S

-- | A tutor is just an index in the table
type Tutor = Int

-- | A date is just an index in the table
type Date = Int

-- | An entry in the registration list. This assigns a tutor a data where that tutor is available.
type Entry = (Tutor,Date)

-- | Lens to the date of an entry
date :: Lens' Entry Date
date = _2

-- | Lens to the tutor of an entry
tutor :: Lens' Entry Tutor
tutor = _1

-- | A registration list contains the set of possible dates for each tutor. A registration list can also
-- hide some dates temporary, so that they appear removed but can be restored later.
data RegistrationList = RegistrationList
                        { _registrationlistEntries     :: S.Set Entry
                        , _registrationlistTutorCount  :: Int
                        , _registrationlistDateCount   :: Int
                        , _registrationlistHiddenDates :: S.Set Date
                        } deriving Show
makeFields ''RegistrationList

-- | Remove a tutor from the list
removeTutor :: Tutor -> RegistrationList -> RegistrationList
removeTutor t = entries %~ S.filter ((t /=) . fst)

-- | Remove a date from the list
removeDate :: Date -> RegistrationList -> RegistrationList
removeDate d = entries %~ S.filter ((d /=) . snd)

-- | Get all entries for a given date
entriesForDate :: Date -> RegistrationList -> [Entry]
entriesForDate d l
  | l^.hiddenDates.contains d = []
  | otherwise = [e | t <- [0..l^.tutorCount - 1], let e = (t,d), l^.entries.contains e]

-- | Get all entries for a given tutor
entriesForTutor :: Tutor -> RegistrationList -> [Entry]
entriesForTutor t l = [e | d <- [0..l^.dateCount - 1], let e = (t,d), l^.entries.contains e, not (l^.hiddenDates.contains d)]

-- | Get all entries for a given date and remove them from the list
viewAndRemoveEntriesForDate :: Date -> RegistrationList -> ([Entry], RegistrationList)
viewAndRemoveEntriesForDate d l = (S.toList e, l & entries .~ r)
    where (e,r) = S.partition ((d ==) . view date) $ l ^. entries 

-- | Get all entries for a given tutor and remove them from the list
viewAndRemoveEntriesForTutor :: Tutor -> RegistrationList -> ([Entry], RegistrationList)
viewAndRemoveEntriesForTutor t l = (S.toList e, l & entries .~ r)
    where (e,r) = S.partition ((t ==) . view tutor) $ l ^. entries 

-- | Check if the given list is empty
empty :: RegistrationList -> Bool
empty l = S.null $ l ^. entries

-- | Hide the given date, so it will be viewed as if it didn't exist
hideDate :: Date -> RegistrationList -> RegistrationList
hideDate d = hiddenDates.contains d .~ True

-- | Show a hidden date again, essentialy restoring the state before the date was hidden.
unhideDate :: Date -> RegistrationList -> RegistrationList
unhideDate d = hiddenDates.contains d .~ False

-- | Read a registration list from a string. The format looks like this:
-- 
-- < Number of tutors >
-- < Dates for the first tutor >
-- < Dates for the second tutor >
-- < ...
readRegistrationList :: String -> Either String RegistrationList
readRegistrationList i = do
  let ls = lines i
  c <- note "Erste Zeile muss Anzahl der Tutoren enthalten" $ headMay ls >>= readMay
  when (null $ take c ls) $ Left "Zu wenig Termindaten"
  es <- S.fromList . concat <$> traverse (uncurry readTutorEntries) (zip [0..pred c] $ take c $ drop 1 ls)
  let dc = succ $ S.findMax $ S.map (view date) es
  return $ RegistrationList es c dc S.empty
  
readTutorEntries :: Tutor -> String -> Either String [(Int,Int)]
readTutorEntries t i = zip (repeat t) <$> traverse (readErr "Ungültige Zahl") (words i)

prettyShowRegistrationList :: RegistrationList -> String
prettyShowRegistrationList l = unlines $ map prettyShowTutor [0..l^.tutorCount - 1]
  where prettyShowTutor t = unwords $ map (prettyShowEntry t) [0..l^.dateCount - 1]
        prettyShowEntry t d
          | l^.entries.contains (t,d) = "X"
          | otherwise = " "
