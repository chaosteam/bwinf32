module Main where

import Common
import Control.Lens
import System.Exit

check :: RegistrationList -> Bool
check l | l ^. tutorCount /= l ^. dateCount - 1 = False
        | otherwise = all check' $ map (\d -> hideDate d l) [0 .. l ^. dateCount - 1]

check' :: RegistrationList -> Bool
check' l | l ^. tutorCount == 0           = True  
         | empty l                       = False
         | l ^. hiddenDates . contains d = check' $ l & dateCount -~ 1  
         | otherwise                     = any check' . map (flip removeTutor l') . map fst $ e
     where (e,l') = viewAndRemoveEntriesForDate d $ l & (dateCount -~ 1) & (tutorCount -~ 1)
           d      = l ^. dateCount - 1

main :: IO ()
main = do
  contents <- getContents
  let l = readRegistrationList contents
  l' <- case l of
    Left err -> putStrLn ("Fehler: " ++ err) >> exitFailure
    Right r  -> return r
  putStrLn $ replicate 80 '-'
  putStrLn "Eingabe: "
  putStrLn $ prettyShowRegistrationList l'
  putStrLn $ replicate 80 '-'
  putStrLn $ if check l' then "Gültig" else "Ungültig"
    
