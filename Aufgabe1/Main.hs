import System.Environment

main :: IO()
main = do
  args <- getArgs
  case args of
    [s]          -> printFraction $ bestFraction 1000 $ parseFraction s
    ["-i", s]    -> printFraction $ (\(a,b) -> bestFraction b (a,b)) $ parseFraction s
    ["-c", x, s] -> printFraction $ bestFraction (10 ^ (read x :: Integer)) $ parseFraction s
    _            -> putStrLn "Error! Wrong Arguments"
  where printFraction (p,q) = putStrLn $ (show p) ++ "/" ++ (show q) 

type Fraction = (Integer, Integer)

parseFraction :: String -> Fraction
parseFraction = (\(b,_:a) -> (read $ b ++ a, 10 ^ length a)) . break (\c -> c == '.' || c == ',')
 
bestFraction :: Integer -> Fraction -> Fraction
bestFraction bound i@(p,q) = snd $ foldr betterFraction (1, i) [1 .. bound]
    where betterFraction n (r, old) | r' < r    = (r', new)
                                    | otherwise = (r, old)
               where r'  = rateFraction new
                     new = (round $ fromIntegral n * d, n)
          d = p `divide` q :: Double
          rateFraction (p',q') = (q' `divide` (2 * bound)) + (abs $ d - (p' `divide` q'))    

divide :: (Integral a, Fractional b) => a -> a -> b
divide x y = (fromIntegral x) / (fromIntegral y)
