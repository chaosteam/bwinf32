module Scoring where

import Control.Lens
import Dancemaster
import Data.Default
import Data.List

-- | Calculate the score of a fake program. 
scoreProgram :: Program  -- ^ Orginal program
      -> Program  -- ^ Fake program
      -> Integer  -- ^ Score
scoreProgram c d = size d * 3 + sum (zipWith scorePosition (positions c) (positions d'))
  where d' = padProgram d $ genericLength $ steps c
        positions x = map (view position) $ runProgram x def
        
-- | Compute the position difference between the orginal and the fake program's position
-- as a score.
scorePosition :: Position  -- ^ Current position of the orginal program 
              -> Position  -- ^ Current position of the fake program
              -> Integer   -- ^ Score
scorePosition p q = max dx dy
  where dy = abs $ p^._X.enum - q^._X.enum
        dx = abs $ p^._Y.enum - q^._Y.enum
