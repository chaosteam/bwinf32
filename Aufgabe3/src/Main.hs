{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Main where

import Dancemaster
import Scoring
import System.Environment
import System.Exit

compileProgram :: String -> IO Program
compileProgram source = case readProgram source of
  Left err -> do
    putStrLn $ "Lesefehler beim Program " ++ source ++ ": \n" ++ show err
    exitFailure
  Right res -> return res

main :: IO ()
main = do
  args <- getArgs
  (originalSource, imitatedSource) <- case args of
    [a,b] -> return (a,b)
    _ -> do
      name <- getProgName
      putStrLn $ "Aufruf: " ++ name ++ " <Original> <Imitat>"
      exitFailure
      
  p1 <- compileProgram originalSource
  p2 <- compileProgram imitatedSource

  putStrLn $ "      Original | " ++ originalSource
  putStrLn $ "        Imitat | " ++ imitatedSource
  putStrLn $ "Strafpunktzahl | " ++ show (scoreProgram p1 p2)
