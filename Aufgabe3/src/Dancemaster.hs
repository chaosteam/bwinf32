{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
module Dancemaster
  ( Dancemaster(..), position, orientation
  , Direction(), dX, dY, rotateRight, rotateLeft
  , Position(..), _X, _Y
  , Move(..), perform, performMoves
  , Program(), size, steps
  , runProgram, readProgram, padProgram
  ) where

import Control.Applicative hiding ((<|>))
import Control.Lens hiding (perform)
import Control.Monad.Trans.State
import Data.Char
import Data.Default
import Data.List
import Text.Parsec
import Text.Parsec.String

--------------------------------------------------------------------------------
-- | A 2D position with X and Y-Coordinates
data Position = Position
  { _x :: Int
  , _y :: Int
  } deriving (Eq, Show)
makeLensesFor [("_x","_X"), ("_y","_Y")] ''Position

-- | The default position is (0|0).
instance Default Position where def = Position def def

-- Note: Don't export setters for dX,dY or the Direction constructor. This ensures that
-- a direction is always a multiple of 45°, because you can only change it by rotating left or right. 
-- | A 2D direction, which should always be a multiple of 45°.
data Direction = Direction
  { dX :: Int
  , dY :: Int
  } deriving (Eq, Show)
             
-- | The default direction is upwards.
instance Default Direction where def = Direction 0 1

-- | Rotate a direction by 45° in clockwise direction. 
rotateRight :: Direction -> Direction
rotateRight (Direction dx dy) 
  | dx == dy = Direction dx 0
  | dx == -dy = Direction 0 dy
  | otherwise = Direction (dx + dy) (dy - dx)

-- | Rotate a direction by 45° in counter-clockwise direction.
rotateLeft :: Direction -> Direction
rotateLeft (Direction dx dy)
  | dx == dy = Direction 0 dy
  | dx == -dy = Direction dx 0
  | otherwise = Direction (dx - dy) (dy + dx)

--------------------------------------------------------------------------------
-- | This stores the state of a robot. A robot has a position and an orientation.
data Dancemaster = Dancemaster
  { _position :: Position
  , _orientation :: Direction
  } deriving (Show, Eq)
makeLenses ''Dancemaster
instance Default Dancemaster where def = Dancemaster def def

--------------------------------------------------------------------------------
-- | Enum of possible moves that a Dancemaster robot can perform.
data Move = Pause | TurnLeft | TurnRight | Forward | Backward deriving Show

-- | Perform a move on a given Dancemaster
perform :: Move -> Dancemaster -> Dancemaster
perform Pause = id
perform Forward = execState $ do
  o <- use orientation
  position . _X += dX o
  position . _Y += dY o
perform Backward = execState $ do
  o <- use orientation
  position . _X -= dX o
  position . _Y -= dY o
perform TurnLeft = orientation %~ rotateLeft
perform TurnRight = orientation %~ rotateRight

-- | Perform multiple moves, returning the Dancemaster state after each move.
performMoves :: Dancemaster -> [Move] -> [Dancemaster]
performMoves = scanl $ flip perform

--------------------------------------------------------------------------------
data Command = Action Move | Repeat Int [Command] deriving Show
data Program = Program Integer [Command] deriving Show

-- | The size of the input program, in characters
size :: Program -> Integer
size (Program s _) = s

-- | Return the moves which a given program encodes. This resolves all repeat commands by just repeating that
-- move sequence. Because of that, the length of the resulting list may not be equal to the size of the program.
steps :: Program -> [Move]
steps (Program _ xs) = go xs
  where go [] = []
        go (Repeat n p : cs) = concat (replicate n (go p)) ++ go cs
        go (Action m : cs) = m : go cs

-- | A Parsec parser for the program.
program :: Parser [Command]
program = many1 $ rep <|> action
  where rep = (Repeat . fromDigit <$> oneOf ['1'..'9'] <*> program <* (char '.'))
        action = try (anyChar >>= maybe (fail "invalid move command") (return . Action) . flip lookup commands)
                 <?> "move command character"
        fromDigit x = ord x - ord '0'
        commands =
          [ ('F', Forward)
          , ('B', Backward)
          , ('l', TurnLeft)
          , ('r', TurnRight)
          , ('-', Pause)
          ]

-- | Parse a program. Return @Right program@ on success, otherwise return @Left err@.
readProgram :: String -> Either ParseError Program
readProgram = fmap . Program . genericLength <*> parse (program <* eof) "<Dancemaster program source>"

-- | Perform a program on a given initial Dancemaster. Returns the state of the dancemaster after each
-- program second.
runProgram :: Program -> Dancemaster -> [Dancemaster]
runProgram p = performMoves ?? steps p

-- | Pad a program to match a given size by inserting pause actions if the program is shorter than
-- the goal size.
padProgram :: Program -- ^ Program to pad
           -> Integer -- ^ Target program size
           -> Program
padProgram p s = Program s $ map Action $ zipWith const (steps p ++ repeat Pause) $ genericReplicate s ()
