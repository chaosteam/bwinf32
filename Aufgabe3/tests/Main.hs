import Test.Tasty
import qualified Assertions
import qualified Properties

main :: IO ()
main = defaultMain $ testGroup "Aufgabe3" 
  [ Assertions.tests
  , Properties.tests
  ] 
