{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Properties where

import Control.Applicative
import Dancemaster
import Data.Default
import Data.Monoid
import Test.QuickCheck
import Test.Tasty.QuickCheck
import Test.Tasty.TH
import Test.Tasty

instance Arbitrary Direction where
  arbitrary = flip appEndo def . mconcat . flip replicate (Endo rotateRight) <$> elements [0..7]
  shrink = const [def]

instance Arbitrary Position where
  arbitrary = liftA2 Position arbitrary arbitrary
  shrink = _X <> _Y $ shrink

instance Arbitrary Dancemaster where
  arbitrary = liftA2 Dancemaster arbitrary arbitrary
  shrink = position shrink <> orientation shrink

prop_rotate_inverse :: Direction -> Property
prop_rotate_inverse d = forAll (elements [1..20]) $ \n ->
  appEndo (mconcat $ replicate n (Endo rotateRight) ++ replicate n (Endo rotateLeft)) d == d

prop_rotate_wrap :: Direction -> Property
prop_rotate_wrap d = forAll (elements [1..7]) $ \n ->
  appEndo (mconcat $ replicate n $ Endo rotateRight) d == appEndo (mconcat $ replicate (8 - n) $ Endo rotateLeft) d

prop_rotate_full :: Direction -> Property
prop_rotate_full d = forAll (elements [1..20]) $ \n ->
  appEndo (mconcat (replicate (8*n) $ Endo rotateRight)) d == d

prop_inverse_move :: Dancemaster -> Bool
prop_inverse_move d = perform Backward (perform Forward d) == d

prop_inverse_turn :: Dancemaster -> Bool
prop_inverse_turn d = perform TurnLeft (perform TurnRight d) == d

prop_identity_move :: Dancemaster -> Bool
prop_identity_move d = perform Pause d == d

tests :: TestTree
tests = $(testGroupGenerator)
