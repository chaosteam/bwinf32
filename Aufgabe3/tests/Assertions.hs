{-# LANGUAGE TemplateHaskell #-}
module Assertions where

import Dancemaster
import Scoring
import Test.HUnit
import Test.Tasty.HUnit
import Test.Tasty.TH

prog :: String -> Program
prog = either (error . show) id . readProgram

case_example1 :: Assertion
case_example1 = 15 @=? scoreProgram real fake
  where real = prog "FrrFrrFrrF"
        fake = prog "4Frr."

case_example2 :: Assertion
case_example2 = 18 @=? scoreProgram real fake
  where real = prog "FrrFrrFrrF"
        fake = prog "F--B"
        
case_solution1 :: Assertion
case_solution1 = 21 @=? scoreProgram real fake
  where real = prog "4FFFFrr."
        fake = prog "44F.rr."

case_solution2 :: Assertion
case_solution2 = 17 @=? scoreProgram real fake
  where real = prog "FrrFllFrrFrrrFF"
        fake = prog "rF"

case_solution3 :: Assertion
case_solution3 = 38 @=? scoreProgram real fake
  where real = prog "lFrrFFllFFFrrFllFrrFF"
        fake = prog "lFr9-F."

tests = $(testGroupGenerator)

